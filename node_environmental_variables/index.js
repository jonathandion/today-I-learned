console.log(process.env.port) // undefined

/**
 * Environmental variables must be fully capitalized.
 */
console.log(process.env.PORT) // 9000
console.log(process.env.NODE_ENV) // development

/**
 *  Nested values are accessibles with an underscore.
 */

// npm ERROR !
// console.log(process.env.MONGO.PASS)

console.log(process.env.MONGO_PASS) // mypass is now accessible.
